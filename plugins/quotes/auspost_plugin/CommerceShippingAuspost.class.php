<?php

class CommerceShippingAuspost extends CommerceShippingQuote {
  /**
   * Settings form to configure Auspost quoting service
   */
  public function settings_form(&$form, $rules_settings) {
    $form['store-info'] = array(
      '#title' => 'Store information',
      '#type' => 'fieldset'
    );
    
    /*
    $form['store-info']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['name']) ? $rules_settings['store-info']['name'] : '',
    );

    $form['store-info']['owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Owner'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['owner']) ? $rules_settings['store-info']['owner'] : '',
    );

    $form['store-info']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['email']) ? $rules_settings['store-info']['email'] : '',
    );

    $form['store-info']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['phone']) ? $rules_settings['store-info']['phone'] : '',
    );

    $form['store-info']['fax'] = array(
      '#type' => 'textfield',
      '#title' => t('Fax'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['fax']) ? $rules_settings['store-info']['fax'] : '',
    );

    $form['store-info']['street1'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #1'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['street1']) ? $rules_settings['store-info']['street1'] : '',
    );

    $form['store-info']['street2'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #2'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['street2']) ? $rules_settings['store-info']['street2'] : '',
    );

    $form['store-info']['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['city']) ? $rules_settings['store-info']['city'] : '',
    );

    $form['store-info']['zone'] = array(
      '#type' => 'textfield',
      '#title' => t('State/Province'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['zone']) ? $rules_settings['store-info']['zone'] : '',
    );
    */
    
    $form['store-info']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal Code'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['postal_code']) ? $rules_settings['store-info']['postal_code'] : '',
    );
    
    /*
    $form['store-info']['country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['store-info']['country']) ? $rules_settings['store-info']['country'] : 'US',
    );
    */
    
    $form['shipment-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Shipment Settings',
    );


    $form['shipment-settings']['auspost_services'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Auspost Services'),
      '#description' => t('Select the Auspost services that are available to customers.'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['auspost_services']) ? $rules_settings['shipment-settings']['auspost_services'] : array(),
      '#options' => _commerce_shipping_auspost_service_list(),
    );

    $form['shipment-settings']['auspost-markup-type'] = array(
      '#type' => 'select',
      '#title' => t('Markup type'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['auspost-markup-type']) ? $rules_settings['shipment-settings']['auspost-markup-type'] : 'percentage',
      '#options' => array(
        'percentage' => t('Percentage (%)'),
        'multiplier' => t('Multiplier (×)'),
        'currency' => t('Addition ($)'),
      ),
    );

    $form['shipment-settings']['auspost-markup'] = array(
      '#type' => 'textfield',
      '#title' => t('Shipping rate markup'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['shipment-settings']['auspost-markup']) ? $rules_settings['shipment-settings']['auspost-markup'] : '0',
      '#description' => t('Markup shipping rate quote by currency amount, percentage, or multiplier.'),
    );

    $form['shipment-settings']['currency_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Currency Code'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['shipment-settings']['currency_code']) ? $rules_settings['shipment-settings']['currency_code'] : 'AUD',
    );

    $form['connection-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Auspost Connection Settings',
    );

    $form['connection-settings']['auspost_connection_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Connection Address'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['connection-settings']['auspost_connection_address']) ? $rules_settings['connection-settings']['auspost_connection_address'] : 'https://auspost.com.au/api/',
      '#description' => t('Include the trailing slash. eg. https://auspost.com.au/api/'),
    );

    $form['connection-settings']['auspost_auth_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Auth key'),
      '#default_value' => is_array($rules_settings) &&
        isset($rules_settings['connection-settings']['auspost_auth_key']) ? $rules_settings['connection-settings']['auspost_auth_key'] : '',
      '#description' => t('The AUTH_KEY for your <a href="!url">Auspost account</a>.', array('!url' => 'http://auspost.com.au/devcentre')),
    );
  }

  /**
   * Checkout Form for selecting Auspost shipment method
   */
  public function submit_form($pane_values, $checkout_pane, $order = NULL) {
    if (empty($order)) {
      $order = $this->order;
    }
    $form = parent::submit_form($pane_values, $checkout_pane, $order);

    // Merge in values from the order.
    if (!empty($order->data['commerce_shipping_auspost'])) {
      $pane_values += $order->data['commerce_shipping_auspost'];
    }
    /* TODO: Find a better way with some nice array function */
    $method = NULL;
    $all_methods = _commerce_shipping_auspost_service_list();
    $methods = array();
    foreach ($this->settings['shipment-settings']['auspost_services'] as $key => $service) {
      if (!$method) 
        $method = $key;
      if ($service !== 0) {
        $methods[$key] = $all_methods[$key];
      }
    }

    // Merge in default values.
    $pane_values += array(
      'method' => $method,
    );

    $form['method'] = array(
      '#type' => 'radios',
      '#title' => t('Shipment Method'),
      '#default_value' => $method,
      '#options' => $methods,
    );

    return $form;
  }

  /**
   * Validation form.
   */
  public function submit_form_validate($pane_form, $pane_values, $form_parents = array(), $order = NULL) {
  }

  /**
   * Calculate Quote
   */
  public function calculate_quote($currency_code, $form_values = array(), $order = NULL, $pane_form = NULL, $pane_values = NULL) {
    $method = $form_values['method'];
    $shipping_address = $pane_values['values']['customer_profile_shipping']['commerce_customer_address'][LANGUAGE_NONE][0];
    $rate = $this->auspost_quote($order, $method, $shipping_address);

    $all_methods = _commerce_shipping_auspost_service_list();

    if (empty($order)) {
      $order = $this->order;
    }
    $settings = $this->settings;
    $shipping_line_items = array();
    $shipping_line_items[] = array(
      'amount' => commerce_currency_decimal_to_amount($rate, $currency_code),
      'currency_code' => $currency_code,
      'label' => t($all_methods[$method]),
    );

    return $shipping_line_items;
  }

  /**
   * Callback for retrieving a Auspost shipping quote.
   *
   * Request a quote for the requested Auspost Service.
   *
   * @param $order
   *   Cart Order.
   *
   * @param $method
   *   The Shipping Method to Quote For
   *
   * @param $shipping_address
   *   The Address to Ship to
   *
   * @return
   *   An array of hotness
   */
  private function auspost_quote($order, $method, $shipping_address) {
    $store_info = $this->settings['store-info'];
    $shipment_settings = $this->settings['shipment-settings'];
    $connection_settings = $this->settings['connection-settings'];

    $shipment_weight = 0;
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $order_line) {
      $line_item_id = $order_line['line_item_id'];
      $line_item = commerce_line_item_load($line_item_id);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);

      $product_weight = isset($product->field_weight[LANGUAGE_NONE]) ? $product->field_weight[LANGUAGE_NONE][0]['value'] : 0;
      $weight = $product_weight * $line_item->quantity;
      $shipment_weight += $weight;
    }

    $ounces = $shipment_weight - floor($shipment_weight);
    $ounces = 16 * $ounces;
    $pounds = floor($shipment_weight);

    $shipto_zip = $shipping_address['postal_code'];
    $shipfrom_zip = $store_info['postal_code'];
    $data = "<Package ID=\"1ST\"><Service>{$method}</Service><ZipOrigination>{$shipfrom_zip}</ZipOrigination><ZipDestination>{$shipto_zip}</ZipDestination><Pounds>{$pounds}</Pounds><Ounces>{$ounces}</Ounces><Size>REGULAR</Size><Machinable>TRUE</Machinable></Package>";
    $request = $this->auspost_access_request($data);
    $resp = drupal_http_request($this->settings['connection-settings']['auspost_connection_address'], array('method' => 'POST', 'data' => $request));

    /**
     * <?xml version="1.0"?> 
     * <RateV3Response>
     *   <Package ID="1ST">
     *     <ZipOrigination>37214</ZipOrigination>
     *     <ZipDestination>37214</ZipDestination>
     *     <Pounds>5</Pounds>
     *     <Ounces>0</Ounces>
     *     <Container></Container>
     *     <Size>REGULAR</Size>
     *     <Zone>1</Zone>
     *     <Postage CLASSID="3">
     *       <MailService>Express Mail&amp;lt;sup&amp;gt;&amp;amp;reg;&amp;lt;/sup&amp;gt;</MailService>
     *       <Rate>19.60</Rate>
     *     </Postage>
     *   </Package>
     * </RateV3Response>
     */

    if ($resp->code == '200' & !empty($resp->data)) {
      $quote = new SimpleXMLElement($resp->data);
      $rate = (Float)$quote->Package->Postage->Rate;
      
      return $rate;
    } else {
      return NULL;
    }
  }



  /**
   * Return XML access request to be prepended to all requests to the Auspost webservice.
   */
  private function auspost_access_request($data) {
    $user_name = $this->settings['connection-settings']['auspost_user_name'];

    return "API=RateV3&XML=<RateV3Request USERID=\"$user_name\">". $data ."</RateV3Request>";
  }

  /**
   * Modify the rate received from Auspost before displaying to the customer.
   */
  private function auspost_markup($rate) {
    $markup = $this->settings['shipment-settings']['auspost-markup'];
    $type = $this->settings['shipment-settings']['auspost-markup-type'];
    if (is_numeric(trim($markup))) {
      switch ($type) {
        case 'percentage':
          return $rate + $rate * floatval(trim($markup)) / 100;
        case 'multiplier':
          return $rate * floatval(trim($markup));
        case 'currency':
          return $rate + floatval(trim($markup));
      }
    }
    else {
      return $rate;
    }
  }

}

/**
 * Convenience function to get Auspost codes for their services.
 */
function _commerce_shipping_auspost_service_list() {
  return array(
    // domestic
    'STANDARD' => t('Standard Delivery'),
    'EXPRESS' => t('Express Post Delivery'),
    // international
    'AIR' => t('International Air Delivery'),
    'SEA' => t('International Surface Delivery'),
    'ECI_D' => t('Express Courier International Document'),
    'ECI_M' => t('Express Courier International Merchandise'),
    'EPI' => t('Express Post International')
  );
}
